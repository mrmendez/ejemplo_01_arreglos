Cuadrado c[];
float lado =40;
public void setup(){
     size(400,200);
     rectMode(CENTER);
     frameRate(7);
     c = new Cuadrado[5];
     for(int i=0;i < c.length;i++)
     c[i] = new Cuadrado(random(width-40)+20, random(height-40), random(40)+15);
}

public void draw(){
     background(127);
     for(int i=0;i < c.length;i++)
     c[i].dibujar();
}
