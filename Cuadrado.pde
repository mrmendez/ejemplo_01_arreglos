
public class Cuadrado{
 PFont p;
 float x;
 float y;
 float lado;
 int numero;
 public Cuadrado(float a, float b, float c){
   this.x = a;
   this.y = b;
   this.lado = c;
   this.numero = (int)random(10);
   p = createFont("Arial", 24);
   textFont(p);
 }

  public void dibujar(){
        fill(255,255,0);
        rect(x,y,lado,lado);
        fill(0);
        text(""+numero,x-5,y+8);
  }


}
